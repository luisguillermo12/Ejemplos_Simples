<?php
/* conversion de tipos de datos int*/

/*enteros . existen 3 formas atraves del conxto , atrves del forzado de tipo , y funciones*/ 

// /*CONTEXTO*/ : el tipo de la variable se definira por el contexto donde la estemos utilizando 
$variable1 = "20  hola mundo <br>";
$suma = 20 + $variable1 ;
echo $suma;
echo gettype($suma);  // esta funcion arroja el tipo de variable q se esta retornando  http://php.net/manual/es/migration71.other-changes.php
//este ejemplo si lo hace 

// este tipo de conversion lo realiza php , no tenemos control del tipo de conversion , en este caso por ser una suma , se necesitan 2 nuemeros y php convierte en un numero la cadena , (ojo en dado caso el nuemero este despues de cualquier letra no se realizara la conversion)
$variable2 = "hola mundo 20 <br>";
$suma = 20 + $variable2 ;
echo $suma;
echo gettype($suma);
 // este ejemplo no lo hace

/* FORZADO DE TIPO


 */ 
$entero =(int)$variable1;
echo $entero;
echo gettype($entero);


// sid eseamos convertir una variable booleana a su equivalente en nuemeros 

$var_bolean = false ;
$var_bolean_2 = true ;

$var_forze = (int)$var_bolean ;
echo $var_forze ;
echo gettype($var_forze);
$var_forze_2 = (int)$var_bolean_2 ;
echo $var_forze_2 ;
echo gettype($var_forze_2);

// si desaamos convertir de float a int ( independientemente de los decimas el coloca es el primer numero NO APROXIMA. NO APROXIMA)

$flo = 8.6;
$var_forze_int =(int)$flo;

echo $var_forze_int ;
echo gettype($var_forze_int);

/* forzado de tipo por funcion */ 

$num_flo = 9.9; 
$num_int_con = intval($num_flo);
echo $num_int_con ;
echo gettype($num_int_con);

/*****************************************************************************************************************************************************************************/
/*conversion a numeros float o dubl  http://php.net/manual/es/migration71.other-changes.php*/   
/*contexo*/
$float1 = "20.5  hola mundo <br>";
$sumafloat = 20 + $float1 ;
echo $sumafloat;
echo gettype($sumafloat);
/*forzado*/
$real =(float)$float1;
echo $real;
echo gettype($real);
/*por funcion */

$num_real_flo = floatval($float1);
echo $num_real_flo;
echo gettype($num_real_flo);

// sid eseamos convertir una variable booleana a su equivalente en nuemeros 

$var_bolean_float = false ;
$var_bolean_2_float = true ;

$var_forze_float = (float)$var_bolean_float ;
echo $var_forze_float ;
echo gettype($var_forze_float);
$var_forze_2_float = (float)$var_bolean_2_float ;
echo $var_forze_2_float ;
echo gettype($var_forze_2_float);


/*****************************************************************************************************************************************************************************/
/*conversion a tipos boleanos*/   
//echo "conversion de tipos";

/* conversion de tipos de variables tipo bolean*/

/* forzado de tipo y funcion

si  lo colocado en la variable es : 0 , 0.0 , "" , 	"0"; imprimira nulo u false 
*/

$variable = "5 hola";
$buleano = (boolean)$variable;
echo $buleano;
echo gettype($buleano);

// conversion a boleano
$buleano2 = boolval($variable) ;
echo $buleano2;
echo gettype($buleano2);

//conversion a tipo array funcion explode 

$numeros = "1,2,3,4,5";
$arraynumeros = explode(",", $numeros, 5); // primer parametro es el separador , el segundo la varible q contiene el array con otro formato , y el ultimo es el limite

echo $arraynumeros [0];
echo $arraynumeros [1]; 

/* forzado a array cuando se realiza el forzado de tipo array solo existira una sola posicion */
$variable2 = "5 hola";
$arrayforze = (array)$variable2;
echo $arrayforze[0];
/* el array forze */

/* de array a string */

$arraycolores = array('amarillo', 'azul', 'rojo');
$string = implode(".", $arraycolores);

echo  $string;


?>

