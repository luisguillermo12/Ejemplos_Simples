<?php
/* varaibles : locales y globales 
locales , php no es un lenguaje fuertemente tipado , lo que quiere decir es que no tienes q  declarar las variables con el tipo de dato que va a contener 
*/

// locales :
 
 $interger = 1 ;
 $float = 1.5 ;
 $bolean = true ;
 $bolean_2 = false ;
 $array_color = array( 'azul','morado', 'negro');
 $string  = "hola";

 echo $interger."<br>";
 echo $float."<br>";
 echo $bolean."<br>";
 echo $bolean_2."<br>"; 
 echo $array_color[0]."<br>"; 
 echo $string."<br>"; 

if ($bolean){

 echo "el boleano es true y me imprimo a mi y a todos ";
 echo $interger."<br>";
 echo $float."<br>";
 echo $bolean."<br>";
 echo $bolean_2."<br>"; 
 echo $array_color[0]."<br>"; 
 echo $string."<br>"; 
}else {

	echo "bolean 1 es falso no imprimo a nadie";
}

	if ($bolean_2){

		echo "bolean 2 es verdadero <br>";
	}else{

		 echo "bolean 2 es falso <br>";
	}

/* variables globales 
recordar q las variables globales tienen alcance fuera de la funcion y en todo el script, 
las locales , su ambito es dentro de la funcion
*/

function VariablesGlobales (){
	$variable_local_funcion = "soy variable LOCAL <br>";
	echo $GLOBALS['global'];
	echo $variable_local_funcion;
}

$global = "soy la variable GLOBAL <br>";

VariablesGlobales ();

echo "VARIABLES SUPER GLOBALES <br>";

//var_dump($_SERVER); "informacion del servidor"
//var_dump($GLOBALS); "variables disponibles"
//var_dump($_COOKIE);
/* VARIABLES SUPER GLOBALES 

$GLOBALS : hace referencia  ala variables disponibles en el ambito global
$_SERVER : informacion del entorno del servidor y ejecucion
$_GET : variabels http get (array de variables get)
$_POST : variables post de http (array de variables post)
$_FILES : variables de subida de ficheros http (array de cosas por subir )
$_COOKIE : cookies http (array de variables de cookies)
$_SESSION : variables de sesion (array de variables de sesion)
$_REQUEST . variables hhtp del request  (verificar que tiene a la hora de mandar un formulario)
$_ENV :  variables de entorno (verificar que tiene a la hora de mandar un formulario)
$php_errormsg : mensaje de error anterior // no sirve
$http_response_header - encabezados de respuesta http // no sirve
$argc  : nuemeros de argumentos pasados a un script  // no sirve
$argv : array de argumentos pasados a un script // no sirve
*/







?>

