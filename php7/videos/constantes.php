<?php
/* CONSTANTES 

* no llevan el signo de dolar 
antes de php 5.3 solo podiamos declararlas con el define()
para definir una constante ahora podemos usar const
* se puede acceder desde cualquir sitio 
* no pueder ser redefinidas 
* no pueden ser eliminadas
* antes de la version 5.6 no podiamos usar un array ahora si 
* en el php 5.6 cono podiamos usar el const , en php 7 podemos usar el const y el define ()

*/ 

// pruebas con const 
 const CONSTANTE = "SOY UNA CONSTANTE";
 const NUMERO = 1;
 const ARREGLO = array('amarillo','azul','rojo' );

 echo CONSTANTE;
 echo NUMERO;
 echo ARREGLO [0];

 // const CONSTANTE  = "OTRO VALOR";   "ESTO ES UN ERROR POR QUE UNA CONSTANTE NO PUEDE REDEFINIDA";

// pruebas con define
 define('CONSTANTE2', '"hola mundo con una constante definida con define ()"');

 echo CONSTANTE2;

 define ('ARREGLO2' , array('amarillo define ','azul define ','rojo define' ));


 echo ARREGLO2[1];


 /*CONSTANTES PREDEFINIDAS 
estan definidas por el nucleo de php
Estas constantes están definidas por el núcleo de PHP. Este incluye PHP, el motor Zend, y los módulos SAPI.

__LINE__	El número de línea actual en el fichero.
__FILE__	Ruta completa y nombre del fichero con enlaces simbólicos resueltos. Si se usa dentro de un include, devolverá el nombre del fichero incluido.
__DIR__	Directorio del fichero. Si se utiliza dentro de un include, devolverá el directorio del fichero incluído. Esta constante es igual que dirname(__FILE__). El nombre del directorio no lleva la barra final a no ser que esté en el directorio root.
__FUNCTION__	Nombre de la función.
__CLASS__	Nombre de la clase. El nombre de la clase incluye el namespace declarado en (p.e.j. Foo\Bar). Tenga en cuenta que a partir de PHP 5.4 __CLASS__ también funciona con traits. Cuando es usado en un método trait, __CLASS__ es el nombre de la clase del trait que está siendo utilizado.
__TRAIT__	El nombre del trait. El nombre del trait incluye el espacio de nombres en el que fue declarado (p.e.j. Foo\Bar).
__METHOD__	Nombre del método de la clase.
__NAMESPACE__	Nombre del espacio de nombres actual.
ClassName::class	El nombre de clase completamente cualificado. Véase también ::class.


PHP_VERSION (string)
La versión actual de PHP en notación "mayor.menor.edición[extra]".
PHP_MAJOR_VERSION (integer)
La versión "mayor" actual de PHP como valor integer (p.ej., int(5) en la versión "5.2.7-extra"). Disponible desde PHP 5.2.7.
PHP_MINOR_VERSION (integer)
La versión "menor" actual de PHP como valor integer (p.ej, int(2) en la versión "5.2.7-extra"). Disponible desde PHP 5.2.7.
PHP_RELEASE_VERSION (integer)
La versión de "publicación" (release) actual de PHP como valor integer (p.ej., int(7) en la versión "5.2.7-extra"). Disponible desde PHP 5.2.7.
PHP_VERSION_ID (integer)
La versión de PHP actual como valor integer, útil para comparar versiones (p.ej., int(50207) para la versión "5.2.7-extra"). Disponible desde PHP 5.2.7.
PHP_EXTRA_VERSION (string)
La versión "extra" actual de PHP, en forma de string (p.ej., '-extra' para la versión "5.2.7-extra"). Se usa a menudo por los distribuidores para indicar la versión de un paquete. Disponible desde PHP 5.2.7.
PHP_ZTS (integer)
Disponible desde PHP 5.2.7.
PHP_DEBUG (integer)
Disponible desde PHP 5.2.7.
PHP_MAXPATHLEN (integer)
La longitud máxima de los nombres de ficheros (incluyendo directorios) admitida por la compilación de PHP. Disponible desde PHP 5.3.0.
PHP_OS (string)
El sistema operativo para el que se construyó PHP.
PHP_OS_FAMILY (string)
La familia de sistemas operativos para la que se construyó PHP. Puede se 'Windows', 'BSD', 'OSX', 'Solaris', 'Linux' or 'Unknown'. Disponible desde PHP 7.2.0.
PHP_SAPI (string)
La API del servidor de la compilación de PHP. Ver también php_sapi_name().
PHP_EOL (string)
El símbolo 'Fin De Línea' correcto de la plataforma en uso. Disponible desde PHP 5.0.2
PHP_INT_MAX (integer)
El número entero más grande admitido en esta compilación de PHP. Normalmente int(2147483647). Disponible desde PHP 5.0.5
PHP_INT_MIN (integer)
El número entero más pequeño admitido en esta compilación de PHP. Normalmente int(-2147483648) en sistemas de 32 bits y int(-9223372036854775808) en sistemas de 64 bits. Disponible desde PHP 7.0.0. Usualmente, PHP_INT_MIN === ~PHP_INT_MAX.
PHP_INT_SIZE (integer)
El tamaño de un número entero en bytes en esta compilación de PHP. Disponible desde PHP 5.0.5
PHP_FLOAT_DIG (integer)
Número de dígitos decimales que se pueden redondear en un float y revertirlos si pédida de precisión. Disponible a partir de PHP 7.2.0.
PHP_FLOAT_EPSILON (float)
El menor número positivo representable x, tal que x + 1.0 != 1.0. Disponible a partir de PHP 7.2.0.
PHP_FLOAT_MIN (float)
El menor número de punto flotante representable. Disponible a partir de PHP 7.2.0.
PHP_FLOAT_MAX (float)
El mayor número de punto flotante representable. Disponible a partir de PHP 7.2.0.
DEFAULT_INCLUDE_PATH (string)
PEAR_INSTALL_DIR (string)
PEAR_EXTENSION_DIR (string)
PHP_EXTENSION_DIR (string)
PHP_PREFIX (string)
El valor de "--prefix" usado en la configuración.
PHP_BINDIR (string)
Especifica dónde se instalaron los binarios.
PHP_BINARY (string)
Especifica la ruta de los binarios de PHP durante la ejecución del script. Disponible desde PHP 5.4.
PHP_MANDIR (string)
Especifica dónde están instalados los manuales. Disponible desde PHP 5.3.7.
PHP_LIBDIR (string)
PHP_DATADIR (string)
PHP_SYSCONFDIR (string)
PHP_LOCALSTATEDIR (string)
PHP_CONFIG_FILE_PATH (string)
PHP_CONFIG_FILE_SCAN_DIR (string)
PHP_SHLIB_SUFFIX (string)
Sufijo de las bibliotecas compartidas de la plataforma de compilación, como "so" (mayoría de plataformas Unix) o "dll" (Windows).
PHP_FD_SETSIZE (string)
El número máximo de descriptores de ficheros para seleccionar llamadas al sistema. Disponble a partir de PHP 7.1.0.
E_ERROR (integer)
Constante de informe de error
E_WARNING (integer)
E_PARSE (integer)
E_NOTICE (integer)
E_CORE_ERROR (integer)
E_CORE_WARNING (integer)
E_COMPILE_ERROR (integer)
E_COMPILE_WARNING (integer)
E_USER_ERROR (integer)
E_USER_WARNING (integer)
E_USER_NOTICE (integer)
E_DEPRECATED (integer)
Disponible desde PHP 5.3.0
E_USER_DEPRECATED (integer)
Disponible desde PHP 5.3.0
E_ALL (integer)
E_STRICT (integer)
__COMPILER_HALT_OFFSET__ (integer)
Disponible desde PHP 5.1.0
TRUE (boolean)
Ver también Booleanos.
FALSE (boolean)
Ver también Booleanos.
NULL (null)
Ver también Null.
 */

ECHO "contantes predefinidas <br>";
echo PHP_INT_MIN."<br>";
echo PHP_FLOAT_DIG;

?>

